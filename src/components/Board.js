import React from "react";

function Square (props) {
    return (
      <button className="square"
      onClick = { props.onClick }>
        {props.value}
      </button>
    );
}

class Board extends React.Component {

  renderSquare(i) {
    return (
      <Square
        value = {this.props.squares[i]}
        onClick = { ()=> this.props.onClick(i) }
      />);
  }

  render() {
    
    return Array(3).fill(null).map( (rowData, rowIndex) => {
      let cellArray = Array(3).fill(rowIndex);
      return ( 
        <div className="board-row" key={rowIndex}>
          { cellArray.map( (data, index)=>
              <span key={data*3+index}>{this.renderSquare(data*3+index)}</span>
          )}
        </div> );
    } );

  }
}

export default Board;