function recordSteps (stepCoordinates, i) {
    let newStep;
    switch (i) {
      case 0:
        newStep = [1, 1];
        break;
      case 1:
        newStep = [2, 1];
        break;
      case 2:
        newStep = [3, 1];
        break;
      case 3:
        newStep = [1, 2];
        break;
      case 4:
        newStep = [2, 2];
        break;
      case 5:
        newStep = [3, 2];
        break;
      case 6:
        newStep = [1, 3];
        break;
      case 7:
        newStep = [2, 3];
        break; 
      case 8:
        newStep = [3, 3];
        break;
      default:
        newStep = ['error', 'error'];
    }
    stepCoordinates.push(newStep)

    return stepCoordinates;
  }

  export default recordSteps;