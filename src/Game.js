import React from 'react';
import Board from './components/Board.js'
import calculateWinner from './components/winner.js';
import recordSteps from './components/RecordSteps.js';
import Sorting from './components/sorting.js';
import Clock from './components/clock.js';

class Game extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
      }],
      stepCoordinates: [],
      buttonStyle: Array(9).fill('normal'),
      xIsNext: true,
      stepNumber: 0,
      gameActive: false,
    };
  }

  handleClick (i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    const buttonStyle = Array(9).fill('normal');
    
    this.setState ( (state) => {
      return {buttonStyle: buttonStyle,};
    });
    
    if ( calculateWinner (squares) || squares[i] ) {
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';

    this.setState({
      stepCoordinates: recordSteps(this.state.stepCoordinates.slice(), i),}); //pushing every step coordinates in a step history list
    if (!this.state.stepNumber) this.setState ({ gameActive: true, });  //activate the timer

    this.setState ({
      history: history.concat([{
        squares: squares, }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext, });
  }

  jumpTo (step) {
    let buttonStyle = Array(9).fill('normal');
    buttonStyle[step] = 'active';
    this.setState ({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
      buttonStyle: buttonStyle,
    });
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner (current.squares);

    let moves = history.map( (step, move) => {
      const desc = move ?
        'Go to move #' + move + '. Col: ' + this.state.stepCoordinates[move-1][0] + ', Row: ' + this.state.stepCoordinates[move-1][1] + '.' :
        'Go to game start';
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)} className={this.state.buttonStyle[move]}>{desc}</button>
        </li>
      );
    });

    //moves = moves.reverse();

    let status;
    if (winner) {
      status = 'Winner: ' + winner;
    }
    else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O' );
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board 
            squares = {current.squares}
            onClick = { i=>this.handleClick(i) }
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
          <Sorting />
        </div>
        <Clock gamesStatus={this.gameActive} />
      </div>
    );
  }
}

export default Game;